FROM alpine/git as builder

ENV TANGO_DB_VER Database-Release-5.16

RUN git clone https://gitlab.com/tango-controls/TangoDatabase.git database \
  && cd database \
  && git checkout ${TANGO_DB_VER} \
  && mkdir -p /dbinit \
  && cp create_db.sql.in /dbinit/create_db.sql \
  && cp create_db_tables.sql.in /dbinit/create_db_tables.sql \
  && cp stored_proc.sql.in /dbinit/stored_proc.sql \
  && sed -i "s|@TANGO_DB_NAME@|tango|g" /dbinit/create_db.sql \
  && sed -i "s|@TANGO_DB_NAME@|tango|g" /dbinit/create_db_tables.sql \
  && sed -i "s|@TANGO_DB_NAME@|tango|g" /dbinit/stored_proc.sql \
  && sed -i "s|^source create_db_tables.sql$|source /docker-entrypoint-initdb.d/create_db_tables.sql|g" /dbinit/create_db.sql \
  && sed -i "s|^source stored_proc.sql$|source /docker-entrypoint-initdb.d/stored_proc.sql|g" /dbinit/create_db.sql \
  && sed -i "/CREATE DATABASE tango;/d" /dbinit/create_db.sql

FROM mysql:5.5

LABEL maintainer="TANGO Controls Team <contact@tango-controls.org>"

ENV MYSQL_DATABASE=tango
ENV MYSQL_USER=tango
ENV MYSQL_PASSWORD=tango

COPY --from=builder /dbinit/create_db.sql \
    /dbinit/create_db_tables.sql \
    /dbinit/stored_proc.sql \
    /docker-entrypoint-initdb.d/
